import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.*;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {
        // Connects to Atlas database instance (MongoDB)
        ConnectionString connectionString = new ConnectionString("mongodb+srv://kappacat:kappacat@clusterea1.zphho.mongodb.net/ITB?retryWrites=true&w=majority");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .serverApi(ServerApi.builder()
                        .version(ServerApiVersion.V1)
                        .build())
                .build();
        MongoClient mongoClient = MongoClients.create(settings);
        MongoDatabase database = mongoClient.getDatabase("ITB");

        // Populate database with default data
        insertBasicData(database);

        // Exercise 1
//        ex1(database);

        // Exercise 2
//        ex2_1(database);
//        ex2_2(database);
//        ex2_3(database);
//        ex2_4(database);

        // Close MongoDB client
        mongoClient.close();
    }

    /**
     * Method to drop STUDENTS table, create it again and populate it with established default data
     * @param db MongoDatabase
     */
    private static void insertBasicData(MongoDatabase db) {
        // Drop STUDENTS table
        System.out.println("Dropping students");
        db.getCollection("students").drop();

        // Get/Create table STUDENTS
        System.out.println("Loading all basic data to insert...");
        MongoCollection<Document> studentsCollection = db.getCollection("students");

        // INSERTS
        // Insert 1 student
        Document student = new Document("_id", new ObjectId());
        student.append("student_id", 10000)
                .append("class_id", 1)
                .append("scores", asList(
                        new Document("type", "exam").append("score", 30.13),
                        new Document("type", "quiz").append("score", 50.20),
                        new Document("type", "homework").append("score", 0.0),
                        new Document("type", "homework").append("score", 33.51))
                );
        studentsCollection.insertOne(student);
        System.out.println("Successfully inserted 1 student.");

        // Insert 2 students at once
        Document student1 = new Document("_id", new ObjectId());
        student1.append("student_id", 10001)
                .append("name", "Pere")
                .append("class_id", 1)
                .append("scores", asList(
                        new Document("type", "exam").append("score", 70.60),
                        new Document("type", "quiz").append("score", 68.20))
                );

        Document student2 = new Document("_id", new ObjectId());
        student2.append("student_id", 10002)
                .append("name", "Javier")
                .append("surname", "Hidalgo")
                .append("class_id", 3)
                .append("scores", asList(
                        new Document("type", "exam").append("score", 50.4),
                        new Document("type", "teamWork").append("score", 50.20))
                );
        List<Document> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        studentsCollection.insertMany(students);
        System.out.println("Successfully inserted " + students.size() + " students.");
    }

    /**
     * Method to insert 2 new students with specific data
     * @param db MongoDatabase
     */
    private static void ex1(MongoDatabase db) {
        // Get/Create table STUDENTS
        MongoCollection<Document> studentsCollection = db.getCollection("students");

        // Estudiant 1
        Document student1 = new Document("_id", new ObjectId());
        student1.append("student_id", 111)
                .append("name", "Alex")
                .append("surname", "Navarro")
                .append("class_id", "DAM")
                .append("group", "2A")
                .append("scores", asList(
                        new Document("type", "exam").append("score", 100),
                        new Document("type", "teamWork").append("score", 50))
                );

        //Estudiant 2
        Document student2 = new Document("_id", new ObjectId());
        student2.append("student_id", 123)
                .append("name", "Albert")
                .append("surname", "Pons")
                .append("class_id", "Undefined")
                .append("group", "2A")
                .append("interests", asList("music", "gym", "code", "electronics")
                );

        List<Document> students = Arrays.asList(student1, student2);
        try {
            studentsCollection.insertMany(students);
            System.out.println("Successfully inserted " + students.size() + " students.");
        } catch (NullPointerException e) {
            throw new RuntimeException("Unable to connect to MongoDB " + db.getName(), e);
        }
    }

    /**
     * Method to select students from "Group" "2A"
     * @param db MongoDatabase
     */
    private static void ex2_1(MongoDatabase db) {
        //Collection
        MongoCollection<Document> studentsCollection = db.getCollection("students");

        //Query
        BasicDBObject eqQuery = new BasicDBObject();
        eqQuery.put("group", new BasicDBObject("$eq", "2A"));

        //Cursor
        FindIterable<Document> cursor = studentsCollection.find(eqQuery);
        System.out.println("Alumnes del grup de 2A:");
        for (Document d : cursor) {
            System.out.println(d);
        }
    }

    /**
     * Method to select students with a score of 100 at the exam
     * @param db MongoDatabase
     */
    private static void ex2_2(MongoDatabase db) {
        //Collection
        MongoCollection<Document> studentsCollection = db.getCollection("students");

        //Query
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<>();
        obj.add(new BasicDBObject("scores.type", "exam"));
        obj.add(new BasicDBObject("scores.score", 100));
        query.put("$and", obj);

        //Cursor
        FindIterable<Document> cursor = studentsCollection.find(query);
        System.out.println("Alumnes amb un 100 al exam:");
        for (Document d : cursor) {
            System.out.println(d);
        }
    }

    /**
     * Method to select students with a score lower than 50 at the exam
     * @param db MongoDatabase
     */
    private static void ex2_3(MongoDatabase db) {
        //Collection
        MongoCollection<Document> studentsCollection = db.getCollection("students");

        //Query
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<>();
        obj.add(new BasicDBObject("scores.type", "exam"));
        obj.add(new BasicDBObject("scores.score", new BasicDBObject("$lt", 50)));
        query.put("$and", obj);

        //Cursor
        FindIterable<Document> cursor = studentsCollection.find(query);
        System.out.println("Alumnes amb menys d'un 50 al exam:");
        for (Document d : cursor) {
            System.out.println(d);
        }
    }

    /**
     * Method to select "Interests" fields from student with ID = 123
     * @param db MongoDatabase
     */
    private static void ex2_4(MongoDatabase db) {
        //Collection
        MongoCollection<Document> studentsCollection = db.getCollection("students");

        //Query
        BasicDBObject query = new BasicDBObject();
        query.put("student_id", new BasicDBObject("$eq", 123));

        //Cursor
        FindIterable<Document> cursor = studentsCollection.find(query);
        System.out.println("Interessos del estudiant amb student_id = 123:");
        for (Document d : cursor) {
            System.out.println("- ID: " + d.get("student_id"));
            System.out.println("- Nom: " + d.get("name"));
            System.out.println("- Interessos: " + d.get("interests"));
        }
    }
}
